<?php
class Mdata extends CI_Model{
	
	function savenewsletter($datanewsletter){
		$this->db->insert('TNEWSLETTER', $datanewsletter);
	}
	
	function getContent($slug){
		$this->db->from('TCONTENT');
		$this->db->where('CONTENTSLUG',$slug);
		$this->db->order_by('CONTENTUPLOADDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	
	function getContentFull(){
		$this->db->from('TCONTENT');
		$this->db->order_by('CONTENTUPLOADDATE','DESC');
		$this->db->order_by('CONTENTID','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	
	function getLive(){
		$this->db->from('TLIVE');
		$this->db->order_by('LIVEDATE','DESC');
		$this->db->order_by('LIVEID','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	
	function getContentPhoto(){
		$this->db->from('TCONTENT');
		$this->db->where('CONTENTTYPE','photo');
		$this->db->order_by('CONTENTUPLOADDATE','DESC');
		$this->db->order_by('CONTENTID','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	function getContentVideo(){
		$this->db->from('TCONTENT');
		$this->db->where('CONTENTTYPE','video');
		$this->db->order_by('CONTENTUPLOADDATE','DESC');
		$this->db->order_by('CONTENTID','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}
	function getContentOther(){
		$this->db->from('TCONTENT');
		$this->db->order_by('CONTENTID',RAND());
		$this->db->limit(4);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}

	function getPhoto(){
		$this->db->from('TCONTENT');
		$this->db->order_by('CONTENTID',RAND());
		$this->db->limit(4);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
	}

	function savePhoto($datacontent){
		$this->db->insert('TCONTENT', $datacontent);
	}

	function deletePhoto($id){
		$this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
	}

	function saveVideo($datacontent){
		$this->db->insert('TCONTENT', $datacontent);
	}

	function deleteVideo($id){
		$this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
	}

	function saveLive($datacontent){
		$this->db->insert('TLIVE', $datacontent);
	}

	function deleteLive($id){
		$this->db->delete('TLIVE', array('LIVEID' => $id)); 
	}
	/*
	CREATE TABLE TNEWSLETTER (
		NEWSLETTERID INT NOT NULL AUTO_INCREMENT,
		EMAIL VARCHAR(255),
		CREATEDATE DATE,
		CREATETIME TIME,
		IPADDRESS VARCHAR(255),
		PRIMARY KEY (NEWSLETTERID)
	);
	
	CREATE TABLE TCONTENT (
		CONTENTID INT NOT NULL AUTO_INCREMENT,
		CONTENTTITLE VARCHAR(255),
		CONTENTSLUG VARCHAR(255),
		CONTENTTYPE VARCHAR(255),
		CONTENTDETAIL TEXT,
		CONTENTUPLOADDATE DATE,
		PRIMARY KEY (CONTENTID)
	);
	
	CONTENTTYPE:
	1. PHOTO
	2. VIDEO
	3. ARTICLE
	
	CREATE TABLE TLIVE (
		LIVEID INT NOT NULL AUTO_INCREMENT,
		LIVEDATE DATE,
		LIVENAME VARCHAR(255),
		LIVELOCATION VARCHAR(255),
		LIVETICKETS VARCHAR(255),
		LIVERSVP VARCHAR(255),
		PRIMARY KEY (LIVEID)
	);
	
	INSERT INTO TLIVE (LIVEDATE,LIVENAME,LIVELOCATION) VALUES ('2015-03-05','Sensation Music Video // Premiere','atlesta.com');
	*/
}	
?>
