<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
		<?php 
			foreach($content as $row): 
			echo $row->CONTENTTITLE.' | Atlesta';
			endforeach;
		?>
	</title>
	<meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="Englsih">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">
	
	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

  </head>
  <body>
	
	<div class="container">
		<br><br>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<?php foreach($content as $row): ?>
					<div class="content text-center">
						<h1><?php echo $row->CONTENTTITLE; ?></h1>
						<br>
						<a class="twitter-share-button"
   href="https://twitter.com/share"
  data-url="<?php echo base_url().'p/'.$row->CONTENTSLUG;?>"
  data-via="atlesta"
  data-text="<?php echo $row->CONTENTTITLE; ?>"
  data-related="twitterdev:Twitter Developer Relations"
  data-count="vertical">
Tweet
</a>
						<br><br>
						<div class="hvr-outline-in" ><img src="<?php echo base_url();?>assets/img/content/<?php echo $row->CONTENTDETAIL; ?>" alt="<?php echo $row->CONTENTTITLE; ?>"></div>
						<br>
					</div>
				
				<?php endforeach; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="content text-center">
					<br><h3>OTHER POST</h3><br>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
				&nbsp;
			</div>
			<?php foreach($contentother as $row): ?>
				<div class="col-lg-2">
					<div class="content text-center">
						<a href="<?php echo base_url();?>p/<?php echo $row->CONTENTSLUG; ?>/" class="hvr-outline-in"><img style="height: 150px; " src="<?php echo base_url();?>assets/img/content/<?php echo $row->CONTENTDETAIL; ?>" alt="<?php echo $row->CONTENTTITLE; ?>"></a>
						
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="content text-center">
					<br><a href="<?php echo base_url();?>home/" class="hvr-shutter-out-horizontal"><button class="btn-submit"><h5>CLOSE</h5></button></a><br><br><br>
				</div>
			</div>
		</div>
		<?php $this->load->view('vfooter');?>
		
	</div>
	<br><br><br>
	
	<br>
	
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
		function goBack() {
		    window.history.back()
		}
	</script>
	
<script>
window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  </body>
</html>