<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atlesta Official Website</title>
    <meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />

	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
   
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  </head>
  <body>
	
					
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4 text-center">
				<div class="logo">
					<img src="<?php echo base_url();?>assets/img/atlesta-logo-white.png">
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-lg-offset-3 text-center">
				<div class="cover">
					<a href="#" class="hvr-outline-in"><img src="<?php echo base_url();?>assets/img/Atlesta Sensation Album Cover.jpg" ></a>
					<br>
					<br>
					<br>
					<h4>BRAND NEW ALBUM<h4>
					<h1><strong>SENSATION</strong></h1>
					<h4>AVAILABLE NOW</h4>
					<br>
					<br>
					<a onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Download itunes', 'Sensation Download itunes']);" href="https://itunes.apple.com/id/album/sensation/id917081601" target="_blank" class="hvr-shutter-out-horizontal"><h5>DOWNLOAD ON<br>ITUNES // AMAZON</h5></a>
				</div>
			</div>
			<div class="col-lg-3 text-center">
				<div class="cover">
					<div class="twitter">
						<?php $this->load->view('vapitwitter');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<div class="cover">
					<h3>
						<div id="dirtysensation" class="hvr-shutter-out-horizontal"><button class="btn-submit" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Dirty Version', 'Sensation Dirty Version']);">DIRTY VERSION</button></div>
						// 
						<div id="cleansensation" class="hvr-shutter-out-horizontal"><button class="btn-submit" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Clean Version', 'Sensation Clean Version']);">CLEAN VERSION</button></div>
					</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="youtube" style="margin:0;">
					<div id="sensation">
						<a href="#" class="hvr-outline-in" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Artwork', 'Sensation Artwork']);"><img src="<?php echo base_url();?>assets/img/sensation-artwork.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-lg-offset-3">
				<div id="dirtybg" style="display:none; z-index:-99; position:absolute;">
					<a href="#" class="hvr-grow-shadow"><img src="<?php echo base_url();?>assets/img/sensation-dirty-version.png" width="100%"></a>
				</div>
			</div>
			<div class="col-lg-3">
				<div id="dirtyvideo" style="display:none;" class="text-center">
					<br><br><br><h4>ATLESTA - SENSATION<br>( DIRTY VERSION )</h4><br>
					<iframe style="float:right;" width="130%" height="215" src="https://www.youtube.com/embed/4JMGx9c0PTA?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-lg-offset-3" >
				<div id="cleanvideo" style="display:none;" class="text-center">
					<br><br><br><br><h4>ATLESTA - SENSATION<br>( CLEAN VERSION )</h4><br>
					<iframe width="130%" height="215" src="https://www.youtube.com/embed/sD2lGUInGg4?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-lg-3">
				<div id="cleanbg" style="display:none; z-index:-99; position:absolute;">
					<a href="#" class="hvr-grow-shadow"><img src="<?php echo base_url();?>assets/img/sensation-clean-version.png" width="100%"></a>
				</div>
			</div>
		</div>			
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="newsletter">
					<div class="form-group">
					    <form action="<?php echo base_url();?>savenewsletter" method="POST">
						<h3>SIGN UP NEWSLETTER</h3>
						<br>
					    <input type="email" class="form-control" name="txtemail" placeholder="ENTER EMAIL" required>
						<br>
						<a href="#" class="hvr-shutter-out-horizontal"><input type="submit" class="btn-submit" value="SUBMIT"></a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="soundcloud">
					<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/183349785&amp;color=000000&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="entersite">
					<a href="<?php echo base_url();?>home/" class="hvr-shutter-out-horizontal" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Enter Site', 'Sensation Enter Site']);"><h1>ENTER SITE</h1></a>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('vfooter'); ?>
	
	<script>
		$("#dirtysensation").click(function(){
			$("#sensation").hide(1000);
			$("#dirtybg").hide(100);
			$("#cleanbg").hide(100);
			$("#dirtyvideo").hide(100);
			$("#cleanvideo").hide(100);
			$("#dirtybg").delay(1000);
			$("#dirtyvideo").delay(1000);
			$("#dirtybg").css("display","block");
			$("#dirtyvideo").css("display","block");
			$("#dirtybg").addClass("slideRight");
			$("#dirtyvideo").addClass("slideLeft");
		});
		$("#cleansensation").click(function(){
			$("#sensation").hide(1000);
			$("#dirtybg").hide(100);
			$("#cleanbg").hide(100);
			$("#dirtyvideo").hide(100);
			$("#cleanvideo").hide(100);
			$("#cleanbg").delay(1000);
			$("#cleanvideo").delay(1000);
			$("#cleanbg").css("display","block");
			$("#cleanvideo").css("display","block");
			$("#cleanbg").addClass("slideLeft");
			$("#cleanvideo").addClass("slideRight");
		});
	</script>
	
    
  </body>
</html>