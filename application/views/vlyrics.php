<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lyrics | Atlesta Official Website</title>
    <meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="Englsih">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

  </head>
  <body>
	<div class="container">
		<?php $this->load->view('vheader');?>
		<br><br>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="cover">
					<div id="aroma" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/AROMA.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Aroma', 'Aroma']);">
					</div>
					<div id="aromalyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>
						AROMA<br>
						<br>
						I just wanna on your knees<br>
						'cause everytime I'll be <br>
						fallin' to the start<br>
						<br>
						I just wanna on your knees<br>
						'cause everyone will be <br>
						fallin' to this song<br>
						<br>
						</p>
						<button id="hidearomalyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="parisweekend" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/PARIS WEEKEND.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Paris Weekend', 'Paris Weekend']);">
					</div>
					<div id="parisweekendlyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>PARIS WEEKEND<br>
<br>
A long the way, A long the way I'm goin' to Paris now<br>
I wanna go, I wanna go into my flyin' car<br>
Just put it all, think what you want inside my shopping bag<br>
I'm on the way to a Paris, Paris, Paris weekend<br>
<br>
So where's my flight? I'm running out the time<br>
And you'll loose your mind 'bout how we mess around<br>
All though it's too late. Trust me, everything just fine with my private jet<br>
Get shot of tequila and the pilot<br>
<br>
Seems like gettin' high, my body shaking.<br>
Oh sir, make me feel good, oh feel good.<br>
You told me "settle down!"<br>
These some wishes outta' plans, Oh baby hold my hand<br>
To make you feel good, oh feel good.<br>
Here we go, goin' to Paris now.<br>
<br>
I get my suit after take a bath with you<br>
Like a celebrity in fancy hotel room<br>
Make out, sounds so good.<br>
Trust me, all the glitz just mine<br>
It's all that what we want<br>
Get insane all night-long<br>
and you said<br>
<br>
C'mon baby playing hard, give it to me if so tight<br>
Make me feel good, oh feel good<br>
Just feel good baby, don't you stop!<br>
These some wishes outta' plans, Oh baby<br>
hold my hand to make you feel good, oh feel good.<br>
Here we go, goin' to Paris now.<br>
						</p>
						<button id="hideparisweekendlyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="capela" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/CAPELLA.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Capella', 'Capella']);">
					</div>
					<div id="capelalyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>CAPELLA<br>
<br>
Stay, Stay, Stay, Stay.<br>
<br>
You and I. we could wander to the west<br>
Yes we don't mind chasing the sun<br>
Well now I see beauty sunset in your face<br>
And that you smiled beneath the stars<br>
<br>
Maybe baby best for you, best for you, best for you to stay<br>
Maybe baby best for you, best for you, best for you to stay<br>
Maybe baby best for now, best for now, you're best I ever had<br>
Maybe baby best for now, best for now, you're best I ever say<br>
<br>
But that's only takes a spark when the memories come to fade<br>
The way we fell apart and we're not to follow in<br>
If this love so meant together<br>
why you can't just make me stay, stay.<br>
<br>
Kiss your lips in the morning<br>
when I wake and feel of<br>
your skin next to mine<br>
In this car we both laughed<br>
about the start<br>
Up in bed we fell in love<br>
<br>
Watch you go,<br>
watch you go,<br>
watch you gone<br>
Let you go,<br>
let you go,<br>
let you gone<br>
<br>
You are my heart<br>
so please you stayed<br>
It's hard to say<br>
It's a fray<br>
						</p>
						<button id="hidecapelalyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="ohyou" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/OH YOU.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Oh You', 'Oh You']);">
					</div>
					<div id="ohyoulyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>OH YOU<br>
<br>
Oh you, oh you<br>
Just feel the night<br>
Oh you, oh you<br>
Turn off the light<br>
Anybody alright?<br>
Take it all to say<br>
<br>
C'mon get me, c'mon get me<br>
In the car you fill out the jar<br>
Where we sing our some<br>
fancy song, yeah.<br>
Can you tell me 'bout the love<br>
That you meant to be<br>
So we're gonna run to see the sun<br>
So we are in love all the time<br>
<br>
Oh you, oh you<br>
Just feel the night<br>
Oh you, oh you<br>
Turn off the light<br>
Anybody alright?<br>
Take it all to say.<br>
<br>
C'mon baby, c'mon baby<br>
Take a ride to the oceanside<br>
Crave a love to my beating heart<br>
It was so real you're the one<br>
who make it clearly.<br>
So we'll gettin' lost on<br>
boulevard. Take a sweet kiss<br>
on the love of mind.<br>
<br>
You're my low lights<br>
ferris wheel now.<br>
You're my glitter in my hollow.<br>
I can't tell you say fallin love<br>
that is maybe just fine.<br>
'cause sometimes that<br>
thing want to know that<br>
I'm not the one to letting go<br>
So what you gonna say<br>
I will fall and watch you feel like<br>
						</p>
						<button id="hideohyoulyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="sensation" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/SENSATION.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Sensation', 'Sensation']);">
					</div>
					<div id="sensationlyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>SENSATION<br>
<br>
In the end of conversation<br>
You're just leaving a sensation<br>
Oh baby c'mon closer to me<br>
Closer to me, closer to me<br>
<br>
All I want is just a pleasure<br>
With an overnight sensation<br>
Baby take it back my money<br>
Back my money, back my money<br>
<br>
I like it when you haunt<br>
Oh baby would you take some night for more<br>
So I could give you anything you want.<br>
Just fall in love and try to make a sound.<br>
It better I love it when you<br>
wonder, wonder this sensation.<br>
Wonder this sensation.<br>
<br>
I don't know what kind of reaction<br>
Please give me some sort of direction<br>
Oh just c'mon closer to me, closer to me.<br>
						</p>
						<button id="hidesensationlyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="ecstasy" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/ECSTASY.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Ecstasy', 'Ecstasy']);">
					</div>
					<div id="ecstasylyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>ECSTASY<br>
<br>
Oh where you gonna go my heart?<br>
Oh are we goin' to the edge?<br>
Oh baby I got lost through the nest <br>
and watch you everyday,<br>
Never outta of my mind<br>
<br>
Cause I'm okay, I'm okay<br>
And I'm alright, I'm alright.<br>
<br>
Baby I just feel good<br>
Baby I just feel good<br>
Just drown me into water<br>
Oh baby I feel drifting<br>
like you say you<br>
Feel good, Baby I just feel good<br>
Just drown me into water<br>
Oh baby I feel drifting<br>
how you make me feel.<br>
<br>
Oh ain't no one gonna drive me in<br>
Slow baby, wait and take my pill<br>
Are you ready for something tonight? <br>
That something like to play.<br>
I'll hold you were mine<br>
						</p>
						<button id="hideecstasylyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="cadillacmodel" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/CADILLAC MODEL.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Cadillac Model', 'Cadillac Model']);">
					</div>
					<div id="cadillacmodellyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>CADILLAC MODEL<br>
<br>
Hey cadillac model need an attention<br>
I wanna laugh loud, I wanna laugh loud<br>
<br>
And she like to show ain't nothing on her mind<br>
And she didn't know 'bout how she's act like bitches, c'mon!<br>
Now I've got the feelin' you pretending I was gone<br>
Just put your lipstick on, walk with your high-heels on.<br>
						</p>
						<button id="hidecadillacmodellyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="perfume" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/PERFUME.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Perfume', 'Perfume']);">
					</div>
					<div id="perfumelyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>PERFUME<br>
<br>
So let me kiss you up and down<br>
Maybe you'll let what I've found<br>
Baby don't you know?<br>
<br>
Take off your dressing gown<br>
Do me whatever you'll sound<br>
We're on hunting ground<br>
<br>
So baby you want me slow down, slow<br>
You'll be fallin' all the time, all the time<br>
Then you baby want me slowing down, slow<br>
You'll be fallin' all the time, all the time<br>
<br>
But something 'bout you is just so right<br>
Maybe it's your smile or maybe it's<br>
your kiss when your lips touched mine<br>
i can smell your perfume<br>
It's your perfume<br>
It's your perfume<br>
<br>
I'm looking up into your eyes<br>
I'm finding out under your blouse<br>
Lacy black in toe<br>
<br>
You said " wanna come inside? "<br>
You moaning like a bona fide<br>
You give it not so tired<br>
						</p>
						<button id="hideperfumelyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="beautifulliedown" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/BEAUTIFUL LIE DOWN.png" onClick="_gaq.push(['_trackEvent', 'Lyrics', 'Beautiful Lie Down', 'Beautiful Lie Down']);">
					</div>
					<div id="beautifulliedownlyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>BEAUTIFUL LIE DOWN<br>
<br>
Isn't over, isn't over<br>
Somebody refuse to tell me<br>
How can I be the one to ignored?<br>
<br>
Isn't broken, isn't broken<br>
Just blinded for me to see<br>
How you can be the right one to feel?<br>
<br>
About how funny we try move on<br>
About how funny we're just hold on<br>
Just call me when it's already gone<br>
The way you kinda be like something,<br>
something, something, something called<br>
Beautiful lie down, beautiful lie down,<br>
you give it to me, Ah lie down,<br>
beautiful lie down, you give it to me<br>
Ah lie down, beautiful lie down,<br>
you give it to me, Ah lie down,<br>
beautiful lie down<br>
I give it to you.<br>
<br>
It's whisper to spoken<br>
You're just trying to hurt me<br>
While we're just making love in July<br>
You're my lover in my blanket<br>
Why can't you just here with me?<br>
We're cuddling the rest of the night<br>
						</p>
						<button id="hidebeautifulliedownlyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
					<div id="joyofmybrokenheart" class="hvr-outline-in text-center">
						<img src="<?php echo base_url();?>assets/img/lyrics/JOY OF MY BROKEN HEART.png" onClick="_gaq.push(['_trackEvent', 'Lyrcis', 'Joy Of My Broken Heart', 'Joy Of My Broken Heart']);">
					</div>
					<div id="joyofmybrokenheartlyrics" class="hvr-overline-from-right text-center" style="display:none;">
						<p>
						<br>JOY OF MY BROKEN HEART<br>
<br>
I'm a lover with a love songs<br>
Hold me tight though your mind <br>
slept in paradise<br>
<br>
So I kept callin'<br>
calling your name<br>
You were mine, we are one <br>
before our time<br>
<br>
Oh baby, Oh baby<br>
you seem not gonna try<br>
gonna try ( gonna try )<br>
<br>
Oh lady, oh lady<br>
you wonder if you cry<br>
if you cry ( don't you cry )<br>
<br>
I will fallin' love<br>
I will fallin' love<br>
I will fallin' love<br>
It's a joy of my broken heart<br>
<br>
Wish I could take you out someday<br>
We could hang in that old boat out near the bay<br>
Once the night falls you said "hang on"<br>
You and me and that boat alone with the stars<br>
						</p>
						<button id="hidejoyofmybrokenheartlyrics" class="btn-close">// CLOSE //</button>
					</div>
					<br><br>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter');?>
	<br>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 20,
                padding_y: 20,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script>
		$("#aroma").click(function(){
			$("#aromalyrics").show(1000);
		});
		$("#hidearomalyrics").click(function(){
			$("#aromalyrics").hide(1000);
		});
		$("#parisweekend").click(function(){
			$("#parisweekendlyrics").show(1000);
		});
		$("#hideparisweekendlyrics").click(function(){
			$("#parisweekendlyrics").hide(1000);
		});
		$("#capela").click(function(){
			$("#capelalyrics").show(1000);
		});
		$("#hidecapelalyrics").click(function(){
			$("#capelalyrics").hide(1000);
		});
		$("#ohyou").click(function(){
			$("#ohyoulyrics").show(1000);
		});
		$("#hideohyoulyrics").click(function(){
			$("#ohyoulyrics").hide(1000);
		});
		$("#sensation").click(function(){
			$("#sensationlyrics").show(1000);
		});
		$("#hidesensationlyrics").click(function(){
			$("#sensationlyrics").hide(1000);
		});
		$("#ecstasy").click(function(){
			$("#ecstasylyrics").show(1000);
		});
		$("#hideecstasylyrics").click(function(){
			$("#ecstasylyrics").hide(1000);
		});
		$("#cadillacmodel").click(function(){
			$("#cadillacmodellyrics").show(1000);
		});
		$("#hidecadillacmodellyrics").click(function(){
			$("#cadillacmodellyrics").hide(1000);
		});
		$("#perfume").click(function(){
			$("#perfumelyrics").show(1000);
		});
		$("#hideperfumelyrics").click(function(){
			$("#perfumelyrics").hide(1000);
		});
		$("#beautifulliedown").click(function(){
			$("#beautifulliedownlyrics").show(1000);
		});
		$("#hidebeautifulliedownlyrics").click(function(){
			$("#beautifulliedownlyrics").hide(1000);
		});
		$("#joyofmybrokenheart").click(function(){
			$("#joyofmybrokenheartlyrics").show(1000);
		});
		$("#hidejoyofmybrokenheartlyrics").click(function(){
			$("#joyofmybrokenheartlyrics").hide(1000);
		});
	</script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  </body>
</html>