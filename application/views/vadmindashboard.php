<!DOCTYPE html>
<html>
<head>    
	<meta charset="utf-8">    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<title><?php echo $title;?></title>   
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">   
	<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body>	
	<div id="wrapper">        
		<?php $this->load->view('vadminmenu');?>        
		<div id="page-wrapper">            
			<div class="row">          
		      	<div class="col-lg-12">   
              		<h1 class="page-header">Dashboard</h1>
            	</div>
            </div>	
			<div class="row"> 
	           	<div class="col-lg-12">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        Hello <?php echo $username; ?>! Welcome to Administrator Page Atlesta Official Website.<br>Powered by <a target="_blank" href="http://sidechainlabs.com/">Sidechain Labs</a>
	                 	</div>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>	
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>   
 	<script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>	
 	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>	
 	<script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();		
		});    
	</script>
</body>
</html>