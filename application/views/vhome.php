<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home | Atlesta Official Website</title>
    <meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="Englsih">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">
	
	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 20,
                padding_y: 20,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  </head>
  <body>

	<div class="container">
		<?php $this->load->view('vheader');?>
		<br><br>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<section id="blog-landing">
					<?php foreach($content as $row): ?>
					<?php if($row->CONTENTTYPE=='photo'){?>
						<article class="white-panel"><a href="<?php echo base_url();?>p/<?php echo $row->CONTENTSLUG; ?>/" class="hvr-outline-in"><img src="<?php echo base_url();?>assets/img/content/<?php echo $row->CONTENTDETAIL; ?>" alt="<?php echo $row->CONTENTTITLE; ?>"></a></article>
					<?php }elseif($row->CONTENTTYPE=='video'){ ?>
						<article class="white-panel"><?php echo $row->CONTENTDETAIL; ?></article>
					<?php } ?>
					<?php endforeach; ?>
					
				</section>
			</div>
		</div>
	</div>
	<br><br><br>
	<?php $this->load->view('vfooter');?>
	<br>
    
  </body>
</html>