<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model("mdata");
		date_default_timezone_set("Asia/Jakarta");
    }
	
	public function index()
	{
		$data['title'] = 'Atlesta';
		$this->load->view('vindex',$data);
	}	
	
	function home()
	{
		$data['title'] = 'Atlesta';
		$total = $this->mdata->getContentFull();
		$data["total_groups"] = count($total)/5;
		$data["content"] = $this->mdata->getContentFull();
		$this->load->view('vhome',$data);
	}
	
	function p($slug='')
	{
		$data['title'] = 'Atlesta';
		$uri1 = $this->uri->segment(1);
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$slug = $uri2;
		$data["content"] = $this->mdata->getContent($slug);
		$data["contentother"] = $this->mdata->getContentOther();
		$this->load->view('vpage',$data);	
	}
	
	function savenewsletter()
	{
		$data['title'] = 'Atlesta';
		$txtemail = $this->input->post('txtemail');
		$ipaddress = $this->input->ip_address();
		$datenow = date("Y-m-d");
		$timenow = date('H:i:s');
		$datanewsletter = array(
			'EMAIL' => $txtemail,
			'CREATEDATE' => $datenow,
			'CREATETIME' => $timenow,
			'IPADDRESS' => $ipaddress
		);
		$this->mdata->savenewsletter($datanewsletter);
		echo '<script>alert("Thanks for Join Our Newsletter!");</script>';
		redirect('','refresh');
	}
	
	function music()
	{
		$data['title'] = 'Atlesta';
		$this->load->view('vmusic',$data);
	}
	
	function photo()
	{
		$data['title'] = 'Atlesta';
		$data["content"] = $this->mdata->getContentPhoto();
		$this->load->view('vphoto',$data);
	}
	
	function video()
	{
		$data['title'] = 'Atlesta';
		$data["content"] = $this->mdata->getContentVideo();
		$this->load->view('vvideo',$data);
	}
	
	function store()
	{
		$data['title'] = 'Atlesta';
		$this->load->view('vstore',$data);
	}
	
	function live()
	{
		$data['title'] = 'Atlesta';
		$data["live"] = $this->mdata->getLive();
		$this->load->view('vlive',$data);
	}
	
	function lyrics()
	{
		$data['title'] = 'Atlesta';
		$this->load->view('vlyrics',$data);
	}
}