<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
    }
	
	function index()
	{
		$data['title'] = 'Atlesta';
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERNAME='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "Atlesta";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Atlesta";
			$data['username'] = $this->session->userdata('username');
			//$data['qdata'] = $this->mdata->getRegistration();
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function photo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Atlesta";
			$data['username'] = $this->session->userdata('username');
			$data['getContentPhoto'] = $this->mdata->getContentPhoto();
			$this->load->view('vadminphoto',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function video()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Atlesta";
			$data['username'] = $this->session->userdata('username');
			$data['getContentVideo'] = $this->mdata->getContentVideo();
			$this->load->view('vadminvideo',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function live()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Atlesta";
			$data['username'] = $this->session->userdata('username');
			$data['getLive'] = $this->mdata->getLive();
			$this->load->view('vadminlive',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function lyric()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Atlesta";
			$data['username'] = $this->session->userdata('username');
			//$data['qdata'] = $this->mdata->getRegistration();
			$this->load->view('vadminlyric',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function addPhoto()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$this->load->view('vadminaddphoto',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function savePhoto()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/img/content/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'Atlesta';
				$this->load->view('vadminaddphoto', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTDETAIL' => $file_name,
					'CONTENTTYPE' => 'photo',
					'CONTENTUPLOADDATE' => $date
				);
				$this->mdata->savePhoto($datacontent);
				$data['getContentPhoto'] = $this->mdata->getContentPhoto();
				$this->load->view('vadminphoto',$data);
			}
		}else{
			redirect('admin','refresh');
		}
	}

	function deletePhoto()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$id = $this->input->post("contentID");
			$deldata = $this->mdata->deletePhoto($id);
			redirect('admin/photo/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$this->load->view('vadminaddvideo',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$youtubecode = $this->input->post('txtyoutubecode');
			$youtube = '<iframe width="100%" height="215" src="https://www.youtube.com/embed/'.$youtubecode.'" frameborder="0" allowfullscreen></iframe>';
			$date = date("Y-m-d");
		
				
			$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTDETAIL' => $youtube,
					'CONTENTTYPE' => 'video',
					'CONTENTUPLOADDATE' => $date
			);
			$this->mdata->saveVideo($datacontent);
			$data['getContentVideo'] = $this->mdata->getContentVideo();
			$this->load->view('vadminvideo',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function deleteVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$id = $this->input->post("contentID");
			$deldata = $this->mdata->deleteVideo($id);
			redirect('admin/video/');
		}else{
			redirect('admin','refresh');
		}
	}

	function addLive()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$this->load->view('vadminaddlive',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveLive()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$name = $this->input->post('txtname');
			$location = $this->input->post('txtlocation');
			$date = $this->input->post('txtdate');
			$tickets = $this->input->post('txttickets');
			$rsvp = $this->input->post('txtrsvp');
			
				
			$datacontent = array(
					'LIVENAME' => $name,
					'LIVELOCATION' => $location,
					'LIVEDATE' => $date,
					'LIVETICKETS' => $tickets,
					'LIVERSVP' => $rsvp
			);
			$this->mdata->saveLive($datacontent);
			$data['getLive'] = $this->mdata->getLive();
			$this->load->view('vadminlive',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function deleteLive()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Atlesta';
			$id = $this->input->post("liveID");
			$deldata = $this->mdata->deleteLive($id);
			redirect('admin/live/');
		}else{
			redirect('admin','refresh');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}
}